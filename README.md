<p align="center">
  <img src="TekandMe.jpg" alt="Tek&Me Logo"  width="100"/>
</p>

<h1 align='center'> <strong> TEK&ME ESLINT & PRETTIER CONFIG </strong></h1>

This is an ESLint and Prettier config to ensure consistent code style and quality throughout the project. These tools help us catch potential errors, enforce best practices, and maintain a clean and readable codebase. The project already includes the necessary configuration files to get started.

<h2><Strong>Let's get Started ⚡🚀: </strong></h2>
<hr>

EsLint us a widely adopted JavaScript linter that hemps us indentify and fix To check your code against the Eslint rules

<h2><Strong>USAGE 👨‍💻:</strong> </h2>
<hr>
<br/>

To ensure code consistency across projects, we have provided an ESLint and Prettier configuration for your convenience. To transfer the configuration to your project, follow these steps:

1. Copy the `.eslintrc` file from the root of this project into the root of your project.

2. Copy the `.prettierrc` file from the root of this project into the root of your project.

3. Update your project's dependencies by running the following command:

   ```shell
   yarn add eslint prettier eslint-plugin-react eslint-plugin-prettier eslint-config-prettier --dev
   ```

4. Update your project's "package.json" file with the following scripts :

```json
"scripts": {
  "lint:check": "eslint .",
  "lint:fix": "eslint . --fix"
}
```

To check for linting errors run the following command:

This command will analyze your code using ESLint and display any linting errors or warnings in the terminal.

```shell
yarn lint:check
```

To automatically fix linting issues, run the following command:
This command will attempt to fix any fixable linting issues and update the files accordingly.

```
yarn lint:fix
```

<br/>
<h2> <strong> Conclusion ☑️ </strong></h2>
<hr/>
In conclusion, the ESLint and Prettier configuration provided in this project has been developed by <strong> Melek Lasosued </strong>.

These configurations help ensure consistent code style, detect potential errors, and enhance the overall code quality of the project. By incorporating ESLint and Prettier, the development team <strong>TEK&ME</strong> can maintain a clean and readable codebase while adhering to best practices.

If you have any further questions or need assistance, please don't hesitate to reach out.
